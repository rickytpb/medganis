# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'creator.ui'
#
# Created by: PyQt5 UI code generator 5.11.3
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_ImageCreator(object):
    def setupUi(self, ImageCreator):
        ImageCreator.setObjectName("ImageCreator")
        ImageCreator.resize(156, 202)
        self.load_bg_button = QtWidgets.QPushButton(ImageCreator)
        self.load_bg_button.setGeometry(QtCore.QRect(10, 10, 131, 23))
        self.load_bg_button.setObjectName("load_bg_button")
        self.remove_col_button = QtWidgets.QPushButton(ImageCreator)
        self.remove_col_button.setGeometry(QtCore.QRect(10, 40, 131, 23))
        self.remove_col_button.setObjectName("remove_col_button")
        self.prepare_bg_button = QtWidgets.QPushButton(ImageCreator)
        self.prepare_bg_button.setGeometry(QtCore.QRect(10, 70, 131, 23))
        self.prepare_bg_button.setObjectName("prepare_bg_button")
        self.load_gan_button = QtWidgets.QPushButton(ImageCreator)
        self.load_gan_button.setGeometry(QtCore.QRect(10, 100, 131, 23))
        self.load_gan_button.setObjectName("load_gan_button")
        self.put_cells_button = QtWidgets.QPushButton(ImageCreator)
        self.put_cells_button.setGeometry(QtCore.QRect(10, 130, 131, 23))
        self.put_cells_button.setObjectName("put_cells_button")
        self.save_image_button = QtWidgets.QPushButton(ImageCreator)
        self.save_image_button.setGeometry(QtCore.QRect(10, 160, 131, 23))
        self.save_image_button.setObjectName("save_image_button")

        self.retranslateUi(ImageCreator)
        QtCore.QMetaObject.connectSlotsByName(ImageCreator)

    def retranslateUi(self, ImageCreator):
        _translate = QtCore.QCoreApplication.translate
        ImageCreator.setWindowTitle(_translate("ImageCreator", "Image Creator"))
        self.load_bg_button.setText(_translate("ImageCreator", "Load background"))
        self.remove_col_button.setText(_translate("ImageCreator", "Remove colors from bg"))
        self.prepare_bg_button.setText(_translate("ImageCreator", "Prepare background"))
        self.load_gan_button.setText(_translate("ImageCreator", "Load GAN"))
        self.put_cells_button.setText(_translate("ImageCreator", "Put cells"))
        self.save_image_button.setText(_translate("ImageCreator", "Save image"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    ImageCreator = QtWidgets.QWidget()
    ui = Ui_ImageCreator()
    ui.setupUi(ImageCreator)
    ImageCreator.show()
    sys.exit(app.exec_())

