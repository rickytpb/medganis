# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'start.ui'
#
# Created by: PyQt5 UI code generator 5.11.3
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_Medganis(object):
    def setupUi(self, Medganis):
        Medganis.setObjectName("Medganis")
        Medganis.resize(399, 413)
        self.verticalLayoutWidget = QtWidgets.QWidget(Medganis)
        self.verticalLayoutWidget.setGeometry(QtCore.QRect(40, 20, 321, 341))
        self.verticalLayoutWidget.setObjectName("verticalLayoutWidget")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.verticalLayoutWidget)
        self.verticalLayout.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout.setObjectName("verticalLayout")
        self.title_label = QtWidgets.QLabel(self.verticalLayoutWidget)
        font = QtGui.QFont()
        font.setFamily("Ubuntu Mono")
        font.setPointSize(28)
        font.setBold(True)
        font.setWeight(75)
        self.title_label.setFont(font)
        self.title_label.setAutoFillBackground(True)
        self.title_label.setAlignment(QtCore.Qt.AlignCenter)
        self.title_label.setObjectName("title_label")
        self.verticalLayout.addWidget(self.title_label)
        self.segmentator_button = QtWidgets.QPushButton(self.verticalLayoutWidget)
        self.segmentator_button.setObjectName("segmentator_button")
        self.verticalLayout.addWidget(self.segmentator_button)
        self.trainer_button = QtWidgets.QPushButton(self.verticalLayoutWidget)
        self.trainer_button.setObjectName("trainer_button")
        self.verticalLayout.addWidget(self.trainer_button)
        self.creator_button = QtWidgets.QPushButton(self.verticalLayoutWidget)
        self.creator_button.setObjectName("creator_button")
        self.verticalLayout.addWidget(self.creator_button)
        self.exit_button = QtWidgets.QPushButton(self.verticalLayoutWidget)
        self.exit_button.setObjectName("exit_button")
        self.verticalLayout.addWidget(self.exit_button)

        self.retranslateUi(Medganis)
        QtCore.QMetaObject.connectSlotsByName(Medganis)

    def retranslateUi(self, Medganis):
        _translate = QtCore.QCoreApplication.translate
        Medganis.setWindowTitle(_translate("Medganis", "Medganis"))
        self.title_label.setText(_translate("Medganis", "Medganis"))
        self.segmentator_button.setText(_translate("Medganis", "Segmentate images"))
        self.trainer_button.setText(_translate("Medganis", "Train new GAN"))
        self.creator_button.setText(_translate("Medganis", "Create ground zero image"))
        self.exit_button.setText(_translate("Medganis", "Exit"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    Medganis = QtWidgets.QWidget()
    ui = Ui_Medganis()
    ui.setupUi(Medganis)
    Medganis.show()
    sys.exit(app.exec_())

