# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'train.ui'
#
# Created by: PyQt5 UI code generator 5.11.3
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_Train(object):
    def setupUi(self, Train):
        Train.setObjectName("Train")
        Train.resize(374, 262)
        self.gridLayoutWidget = QtWidgets.QWidget(Train)
        self.gridLayoutWidget.setGeometry(QtCore.QRect(20, 20, 331, 221))
        self.gridLayoutWidget.setObjectName("gridLayoutWidget")
        self.gridLayout = QtWidgets.QGridLayout(self.gridLayoutWidget)
        self.gridLayout.setContentsMargins(0, 0, 0, 0)
        self.gridLayout.setObjectName("gridLayout")
        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.verticalLayout.setObjectName("verticalLayout")
        self.create_button = QtWidgets.QPushButton(self.gridLayoutWidget)
        self.create_button.setObjectName("create_button")
        self.verticalLayout.addWidget(self.create_button)
        self.load_objs_button = QtWidgets.QPushButton(self.gridLayoutWidget)
        self.load_objs_button.setEnabled(False)
        self.load_objs_button.setObjectName("load_objs_button")
        self.verticalLayout.addWidget(self.load_objs_button)
        self.save_net_button = QtWidgets.QPushButton(self.gridLayoutWidget)
        self.save_net_button.setEnabled(False)
        self.save_net_button.setObjectName("save_net_button")
        self.verticalLayout.addWidget(self.save_net_button)
        self.line = QtWidgets.QFrame(self.gridLayoutWidget)
        self.line.setFrameShape(QtWidgets.QFrame.HLine)
        self.line.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line.setObjectName("line")
        self.verticalLayout.addWidget(self.line)
        self.formLayout = QtWidgets.QFormLayout()
        self.formLayout.setObjectName("formLayout")
        self.bsize_label = QtWidgets.QLabel(self.gridLayoutWidget)
        self.bsize_label.setObjectName("bsize_label")
        self.formLayout.setWidget(3, QtWidgets.QFormLayout.LabelRole, self.bsize_label)
        self.bsize_field = QtWidgets.QLineEdit(self.gridLayoutWidget)
        self.bsize_field.setObjectName("bsize_field")
        self.formLayout.setWidget(3, QtWidgets.QFormLayout.FieldRole, self.bsize_field)
        self.epochs_label = QtWidgets.QLabel(self.gridLayoutWidget)
        self.epochs_label.setObjectName("epochs_label")
        self.formLayout.setWidget(2, QtWidgets.QFormLayout.LabelRole, self.epochs_label)
        self.epochs_field = QtWidgets.QLineEdit(self.gridLayoutWidget)
        self.epochs_field.setObjectName("epochs_field")
        self.formLayout.setWidget(2, QtWidgets.QFormLayout.FieldRole, self.epochs_field)
        self.saveint_label = QtWidgets.QLabel(self.gridLayoutWidget)
        self.saveint_label.setObjectName("saveint_label")
        self.formLayout.setWidget(4, QtWidgets.QFormLayout.LabelRole, self.saveint_label)
        self.saveint_field = QtWidgets.QLineEdit(self.gridLayoutWidget)
        self.saveint_field.setObjectName("saveint_field")
        self.formLayout.setWidget(4, QtWidgets.QFormLayout.FieldRole, self.saveint_field)
        self.train_button = QtWidgets.QPushButton(self.gridLayoutWidget)
        self.train_button.setEnabled(False)
        self.train_button.setObjectName("train_button")
        self.formLayout.setWidget(5, QtWidgets.QFormLayout.SpanningRole, self.train_button)
        self.verticalLayout.addLayout(self.formLayout)
        self.gridLayout.addLayout(self.verticalLayout, 0, 0, 1, 1)

        self.retranslateUi(Train)
        QtCore.QMetaObject.connectSlotsByName(Train)

    def retranslateUi(self, Train):
        _translate = QtCore.QCoreApplication.translate
        Train.setWindowTitle(_translate("Train", "Train new GAN"))
        self.create_button.setText(_translate("Train", "Create new network"))
        self.load_objs_button.setText(_translate("Train", "Load object images"))
        self.save_net_button.setText(_translate("Train", "Save trained network"))
        self.bsize_label.setText(_translate("Train", "Batch size:"))
        self.bsize_field.setText(_translate("Train", "32"))
        self.epochs_label.setText(_translate("Train", "Epochs:"))
        self.epochs_field.setText(_translate("Train", "1000"))
        self.saveint_label.setText(_translate("Train", "Save interval"))
        self.saveint_field.setText(_translate("Train", "200"))
        self.train_button.setText(_translate("Train", "Train network"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    Train = QtWidgets.QWidget()
    ui = Ui_Train()
    ui.setupUi(Train)
    Train.show()
    sys.exit(app.exec_())

