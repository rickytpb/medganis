from ui.start import Ui_Medganis
from ui.imageseg import Ui_ImageSeg
from ui.train import Ui_Train
from ui.creator import Ui_ImageCreator
from image_ops.image_segmentator import ImageSegmentator
from image_ops.image_creator import ImageCreator, MockGenerator
from gan.gan_engine import GAN, get_train_data
from PyQt5 import QtCore, QtGui, QtWidgets
from utils import except_hook
from threading import Thread
import time
import sys
import cv2
import os

class MainApp(object):

    def __init__(self):
        sys.excepthook = except_hook
        self.__segmentator = None
        self.__creator = None
        self.__GAN = None
        self.__GAN_data = None
        self.__cwd = str(os.getcwd())
        self.__temp = None
        self.__set_tmp()
        self.__resize_ratio = 1.
        self.app = QtWidgets.QApplication(sys.argv)
        self.start_menu = QtWidgets.QWidget()
        self.start_ui = Ui_Medganis()
        self.start_ui.setupUi(self.start_menu)
        self.start_menu.show()
        self.__connect_start_menu()
        self.__seg_ui = None
        self.__seg_ui_state = [None] * 4
        self.__creator_ui = None
        self.__creator_ui_state = [None] * 6
        self.__train_ui = None
        self.__train_ui_state = [None] * 4
        sys.exit(self.app.exec_())

    def __remember_state_all(self):
        if self.__seg_ui:
            self.__seg_ui_state[0] = self.__seg_ui.separate_save_button.isEnabled()
            self.__seg_ui_state[1] = self.__seg_ui.changew_cwd_button.isEnabled()
            self.__seg_ui_state[2] = self.__seg_ui.extract_save_button.isEnabled()
            self.__seg_ui_state[3] = self.__seg_ui.load_button.isEnabled()
        if self.__creator_ui:
            self.__creator_ui_state[0] = self.__creator_ui.load_bg_button.isEnabled()
            self.__creator_ui_state[1] = self.__creator_ui.load_gan_button.isEnabled()
            self.__creator_ui_state[2] = self.__creator_ui.prepare_bg_button.isEnabled()
            self.__creator_ui_state[3] = self.__creator_ui.put_cells_button.isEnabled()
            self.__creator_ui_state[4] = self.__creator_ui.remove_col_button.isEnabled()
            self.__creator_ui_state[5] = self.__creator_ui.save_image_button.isEnabled()
        if self.__train_ui:
            self.__train_ui_state[0] = self.__train_ui.create_button.isEnabled()
            self.__train_ui_state[1] = self.__train_ui.load_objs_button.isEnabled()
            self.__train_ui_state[2] = self.__train_ui.save_net_button.isEnabled()
            self.__train_ui_state[3] = self.__train_ui.train_button.isEnabled()

    def __redo_state_all(self):
        if self.__seg_ui:
            self.__seg_ui.separate_save_button.setEnabled(self.__seg_ui_state[0])
            self.__seg_ui.changew_cwd_button.setEnabled(self.__seg_ui_state[1])
            self.__seg_ui.extract_save_button.setEnabled(self.__seg_ui_state[2])
            self.__seg_ui.load_button.setEnabled(self.__seg_ui_state[3])
        if self.__creator_ui:
            self.__creator_ui.load_bg_button.setEnabled(self.__creator_ui_state[0])
            self.__creator_ui.load_gan_button.setEnabled(self.__creator_ui_state[1])
            self.__creator_ui.prepare_bg_button.setEnabled(self.__creator_ui_state[2])
            self.__creator_ui.put_cells_button.setEnabled(self.__creator_ui_state[3])
            self.__creator_ui.remove_col_button.setEnabled(self.__creator_ui_state[4])
            self.__creator_ui.save_image_button.setEnabled(self.__creator_ui_state[5])
        if self.__train_ui:
            self.__train_ui.create_button.setEnabled(self.__train_ui_state[0])
            self.__train_ui.load_objs_button.setEnabled(self.__train_ui_state[1])
            self.__train_ui.save_net_button.setEnabled(self.__train_ui_state[2])
            self.__train_ui.train_button.setEnabled(self.__train_ui_state[3])

    def __set_all(self,value):
        if self.__seg_ui:
            self.__seg_ui.separate_save_button.setEnabled(value)
            self.__seg_ui.changew_cwd_button.setEnabled(value)
            self.__seg_ui.extract_save_button.setEnabled(value)
            self.__seg_ui.load_button.setEnabled(value)
        if self.__creator_ui:
            self.__creator_ui.load_bg_button.setEnabled(value)
            self.__creator_ui.load_gan_button.setEnabled(value)
            self.__creator_ui.prepare_bg_button.setEnabled(value)
            self.__creator_ui.put_cells_button.setEnabled(value)
            self.__creator_ui.remove_col_button.setEnabled(value)
            self.__creator_ui.save_image_button.setEnabled(value)
        if self.__train_ui:
            self.__train_ui.create_button.setEnabled(value)
            self.__train_ui.load_objs_button.setEnabled(value)
            self.__train_ui.save_net_button.setEnabled(value)
            self.__train_ui.train_button.setEnabled(value)


    def __set_tmp(self):
        self.__temp = os.path.join(os.getcwd(),"tmp")
        if not os.path.exists("tmp"):
            os.makedirs("tmp")

    def __connect_start_menu(self):
        self.start_ui.segmentator_button.clicked.connect(self.__start_segmentator)
        self.start_ui.trainer_button.clicked.connect(self.__start_trainer)
        self.start_ui.creator_button.clicked.connect(self.__start_creator)
        self.start_ui.exit_button.clicked.connect(lambda: sys.exit(0))

    def __start_creator(self):
        self.__creator = ImageCreator()
        self.__creator_menu = QtWidgets.QWidget()
        self.__creator_ui = Ui_ImageCreator()
        self.__creator_ui.setupUi(self.__creator_menu)
        self.__creator_ui.load_bg_button.clicked.connect(self.__load_background)
        self.__creator_ui.remove_col_button.clicked.connect(self.__remove_colors_from_bg)
        self.__creator_ui.prepare_bg_button.clicked.connect(self.__prepare_background)
        self.__creator_ui.load_gan_button.clicked.connect(self.__load_gan)
        self.__creator_ui.put_cells_button.clicked.connect(self.__put_cells_on_bg)
        self.__creator_ui.save_image_button.clicked.connect(self.__save_final_image)
        self.__creator_menu.show()

    def __save_final_image(self):
        name, _ = QtWidgets.QFileDialog.getSaveFileName(self.__creator_menu, 'Save Image')
        self.__creator.save_background(name)

    def __put_cells_on_bg_thread(self,positions_stat):
        self.__creator.put_cells(positions_stat,self.__GAN)
        self.__redo_state_all()

    def __put_cells_on_bg(self):
        fileName, _ = QtWidgets.QFileDialog.getOpenFileName(self.__creator_menu, "Load file with positions", "")
        if fileName:
            positions = MockGenerator.load_positions(fileName)
            positions_stat = MockGenerator.get_stat_positions(positions)
            self.__remember_state_all()
            self.__set_all(False)
            t = Thread(target=self.__put_cells_on_bg_thread,args=(positions_stat,))
            t.start()

    def __load_gan_thread(self,fileName):
        self.__GAN = GAN()
        self.__GAN.load_generator(fileName)
        self.__redo_state_all()

    def __load_gan(self):
        fileName, _ = QtWidgets.QFileDialog.getOpenFileName(self.__creator_menu, "Load pretrained GAN", "")
        if fileName:
            self.__remember_state_all()
            self.__set_all(False)
            t = Thread(target=self.__load_gan_thread,args=(fileName,))
            t.start()

    def __load_background_thread(self,fileName):
        self.__creator.load_background(fileName)
        self.__redo_state_all()

    def __load_background(self):
        fileName, _ = QtWidgets.QFileDialog.getOpenFileName(self.__creator_menu, "Load background image", "")
        if fileName:
            self.__remember_state_all()
            self.__set_all(False)
            t = Thread(target=self.__load_background_thread,args=(fileName,))
            t.start()

    def __remove_colors_from_bg_thread(self,fileName,ratio):
        self.__creator.remove_colors(fileName,ratio)
        self.__redo_state_all()

    def __remove_colors_from_bg(self):
        fileName, _ = QtWidgets.QFileDialog.getOpenFileName(self.__creator_menu, "Load image with separated objects", "")
        if fileName:
            ratio, okPressed = QtWidgets.QInputDialog.getDouble(self.__creator_menu, "Decolorize image", "Decolor ratio: ",0.5)
            if okPressed and ratio != 0:
                self.__remember_state_all()
                self.__set_all(False)
                t = Thread(target=self.__remove_colors_from_bg_thread,args=(fileName,ratio,))
                t.start()

    def __prepare_background_thread(self,x,y):
        self.__creator.prepare_background((x,y))
        self.__redo_state_all()

    def __prepare_background(self):
        kernel, okPressed = QtWidgets.QInputDialog.getText(self.__creator_menu, "Blur background", "Kernel(in x,y format): ",
                                                            QtWidgets.QLineEdit.Normal, "5,5")

        if okPressed and kernel != "":
            kernel_arr = str(kernel).split(',')
            x = int(kernel_arr[0])
            y = int(kernel_arr[1])
            self.__remember_state_all()
            self.__set_all(False)
            t = Thread(target=self.__prepare_background_thread,args=(x,y,))
            t.start()


    def __start_segmentator(self):
        self.__segmentator = ImageSegmentator()
        self.__seg_menu = QtWidgets.QWidget()
        self.__seg_ui = Ui_ImageSeg()
        self.__seg_ui.setupUi(self.__seg_menu)
        self.__seg_ui.max_area_field.setText(str(self.__segmentator.get_max_area()))
        self.__seg_ui.min_area_field.setText(str(self.__segmentator.get_min_area()))
        self.__seg_ui.debug_box.stateChanged.connect(self.__debug_set)
        self.__seg_ui.visual_box.stateChanged.connect(self.__visual_set)
        self.__seg_ui.load_button.clicked.connect(self.__load_image)
        self.__seg_ui.separate_save_button.clicked.connect(self.__separate_save)
        self.__seg_ui.extract_save_button.clicked.connect(self.__extract_save_objects)
        self.__seg_ui.changew_cwd_button.clicked.connect(self.__change_cwd)
        self.__seg_ui.resize_ratio_field.textChanged.connect(self.__update_ratio)
        self.__resize_ratio = float(self.__seg_ui.resize_ratio_field.text())
        self.__seg_ui.cwd_label.setText(self.__compose_dir(self.__cwd))
        self.__seg_menu.show()

    def __start_trainer(self):
        self.__train_menu = QtWidgets.QWidget()
        self.__train_ui = Ui_Train()
        self.__train_ui.setupUi(self.__train_menu)
        self.__train_ui.create_button.clicked.connect(self.__create_gan)
        self.__train_ui.load_objs_button.clicked.connect(self.__load_objs_for_gan)
        self.__train_ui.train_button.clicked.connect(self.__train_gan)
        self.__train_ui.save_net_button.clicked.connect(self.__save_GAN)
        self.__train_menu.show()

    def __create_gan_thread(self):
        self.__GAN = GAN()
        self.__redo_state_all()
        self.__train_ui.load_objs_button.setEnabled(True)

    def __create_gan(self):
        self.__remember_state_all()
        self.__set_all(False)
        t = Thread(target=self.__create_gan_thread)
        t.start()

    def __load_objs_for_gan_thread(self,fileNames):
        self.__GAN_data = get_train_data(fileNames)
        self.__redo_state_all()
        self.__train_ui.train_button.setEnabled(True)

    def __load_objs_for_gan(self):
        fileNames, _ = QtWidgets.QFileDialog.getOpenFileNames(self.__train_menu, "QFileDialog.getOpenFileName()", "")
        if len(fileNames):
            self.__remember_state_all()
            self.__set_all(False)
            t = Thread(target=self.__load_objs_for_gan_thread,args=(fileNames,))
            t.start()

    def __train_gan_thread(self,ep,bsize,sint):
        self.__GAN.train(train_data=self.__GAN_data, temp_dir=self.__temp,
                         epochs=ep, batch_size=bsize, save_interval=sint)
        self.__redo_state_all()
        self.__train_ui.save_net_button.setEnabled(True)


    def __train_gan(self):
        ep = int(self.__train_ui.epochs_field.text())
        bsize = int(self.__train_ui.bsize_field.text())
        sint =  int(self.__train_ui.saveint_field.text())

        if self.__GAN:
            self.__remember_state_all()
            self.__set_all(False)
            t = Thread(target=self.__train_gan_thread,args=(ep,bsize,sint,))
            t.start()


    def __save_GAN_thread(self,name):
        self.__GAN.save_generator(name)
        self.__redo_state_all()

    def __save_GAN(self):
        name, _ = QtWidgets.QFileDialog.getSaveFileName(self.__train_menu, 'Save File')
        self.__remember_state_all()
        self.__set_all(False)
        t = Thread(target=self.__save_GAN_thread,args=(name,))
        t.start()


    def __extract_save_objects_thread(self,fileName,label):
        self.__segmentator.set_max_area(int(self.__seg_ui.max_area_field.text()))
        self.__segmentator.set_min_area(int(self.__seg_ui.min_area_field.text()))
        img = cv2.imread(fileName)
        count = self.__segmentator.segmentate_image(img, label, dirname="tmp")
        self.__seg_ui.obj_count_label.setText("Found %d objects" % count)
        self.__segmentator.reset_obj_counter()
        self.__segmentator.save_positions(label, dirname="tmp")
        self.__redo_state_all()

    def __extract_save_objects(self):
        fileName, _ = QtWidgets.QFileDialog.getOpenFileName(self.__seg_menu, "Select image to extract objects from: ", "")
        if fileName:
            label, okPressed = QtWidgets.QInputDialog.getText(self.__seg_menu, "Label for objects", "Your label:", QtWidgets.QLineEdit.Normal, "")
            label = label + "_" + str(int(time.time())) + "_"
            if okPressed and label!="":
                self.__remember_state_all()
                self.__set_all(False)
                t = Thread(target=self.__extract_save_objects_thread,args=(fileName,label,))
                t.start()

    def __change_cwd(self):
        self.__cwd = str(QtWidgets.QFileDialog.getExistingDirectory(self.__seg_menu, "Select Directory",""))
        self.__seg_ui.cwd_label.setText(self.__compose_dir(self.__cwd))
        self.__set_tmp()

    @staticmethod
    def __compose_dir(dir):
        return str("Current working directory is " + str(dir))

    def __update_ratio(self):
        self.__resize_ratio = float(self.__seg_ui.resize_ratio_field.text())

    def __debug_set(self,state):
        if state == QtCore.Qt.Checked:
            self.__segmentator.set_debug_mode(True)
        else:
            self.__segmentator.set_debug_mode(False)

    def __visual_set(self, state):
        if state == QtCore.Qt.Checked:
            self.__segmentator.set_visual_mode(True)
        else:
            self.__segmentator.set_visual_mode(False)

    def __load_image_thread(self,fileName):
        self.__segmentator.load_image(fileName,self.__resize_ratio)
        self.__redo_state_all()
        self.__seg_ui.separate_save_button.setEnabled(True)

    def __load_image(self):
        fileName, _ = QtWidgets.QFileDialog.getOpenFileName(self.__seg_menu, "Load image for segmentation", "")
        if fileName:
            self.__remember_state_all()
            self.__set_all(False)
            t = Thread(target=self.__load_image_thread, args=(fileName,))
            t.start()
            self.__seg_ui.obj_count_label.setText("Loaded file: " + fileName)


    def __separate_save_thread(self):
        self.__segmentator.prepare_image(self.__get_channels_from_string(self.__seg_ui.channel_field.text()),
                                         int(self.__seg_ui.corr_field.text()), int(self.__seg_ui.thresh_field.text()))
        self.__segmentator.separate_image(int(self.__seg_ui.n_cluster_field.text()))
        self.__segmentator.save_prepared_images(self.__cwd)
        self.__redo_state_all()

    def __separate_save(self):
        self.__remember_state_all()
        self.__set_all(False)
        t1 = Thread(target=self.__separate_save_thread)
        t1.start()

    @staticmethod
    def __get_channels_from_string(string):
        channels = []
        string = str(string).upper()
        if "R" in string:
            channels.append(2)
        if "G" in string:
            channels.append(1)
        if "B" in string:
            channels.append(0)
        return channels


if __name__ == "__main__":
    MainApp()



