import cv2
import numpy as np
from utils import show_wait

img = cv2.imread("test.png")
print(img.shape)
lower = np.array([156.,142.,132.])
upper = np.array([186.,162.,142.])
lut = np.zeros((256, 1, 3), dtype=np.uint8)
for i in range(len(lower)):
    arr = np.linspace(lower[i], upper[i], num=256, dtype="uint8")
    lut[:,0,i] = arr
print(lut.shape)
print(lut)
cv2.LUT(img,lut)
show_wait(img,"TEST")
# gan_blue = GAN()
# gan_blue.load_generator("blue_gen.h5")
# obj = cv2.cvtColor(gan_blue.generateObject(), cv2.COLOR_BGR2GRAY)
# obj = (obj * 255 / np.max(obj)).astype('uint8')
# cv2.imwrite("test.png",obj)

# obj = cv2.imread("test.png",1)
# obj = cv2.morphologyEx(obj, cv2.MORPH_OPEN, (5,5))
# show_wait(obj,"OBJ")
# obj = cv2.applyColorMap(obj, cv2.COLORMAP_OCEAN)
# show_wait(obj,"OBJ")
# obj = cv2.GaussianBlur(obj,(3,3),0)
# show_wait(obj,"OBJ")
