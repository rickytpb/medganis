import cv2
import numpy as np
from utils import show_wait

img = cv2.imread("tmp/blue_16.png")
blur = cv2.GaussianBlur(img,(3,3),0)
smooth = cv2.addWeighted(blur,1.5,img,-0.5,0)
show_wait(smooth,"test")
