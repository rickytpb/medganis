import cv2
import numpy as np
from utils import show_wait
import json
from gan.gan_engine import GAN
from skimage import img_as_ubyte
import sys

class ImageCreator(object):
    def __init__(self):
        self.__background = None

    def load_background(self,impath):
        self.__background = cv2.imread(impath,1)
        self.__background = cv2.resize(self.__background,None,fx=1.,fy=1.)

    def prepare_background(self,kernel=(3,3)):
        blur_bg = cv2.GaussianBlur(self.__background, kernel, 0)
        show_wait(blur_bg,"Blurred")

    def __get_mean_color(self,impath, ratio):
        img = cv2.imread(impath,1)
        img = np.array(img)
        img = np.array(img,dtype=np.float32)
        img[np.where(img == [255,255,255])] = np.nan
        lower_bound = []
        upper_bound = []
        for i in range(3):
            mean = np.nanmean(img[:,:,i])
            std = np.nanstd(img[:,:,i])
            low = mean-(std/ratio)
            up = mean+(std/ratio)
            if low<0.:
                low = 0.
            if up>255.:
                up = 255.
            lower_bound.append(low)
            upper_bound.append(up)
        return np.array(lower_bound), np.array(upper_bound)

    def remove_colors(self,impath,ratio=0.5):
        lower,upper = self.__get_mean_color(impath,ratio)
        mask = cv2.inRange(self.__background, lower, upper)
        mask_inv = cv2.bitwise_not(mask)
        self.__background = cv2.bitwise_and(self.__background, self.__background, mask=mask_inv)
        self.__background[np.where(self.__background==[0,0,0])] = 255
        show_wait(self.__background,"Background after color remove")

    def put_cells(self,positions,image_generator):
        for position in positions:
            position[1][0] += 10
            position[1][1] += 10
            size = tuple(position[1])
            position = tuple(position[0])
            obj = self.__prepare_object(image_generator.generateObject())
            obj = cv2.resize(obj,size)
            mask = 255 * np.ones(obj.shape,obj.dtype)
            try:
                self.__background = cv2.seamlessClone(obj,self.__background,mask,position,cv2.MIXED_CLONE)
            except cv2.error as e:
                print(e,file=sys.stderr)
                continue

        show_wait(self.__background,"TEST")

    def __prepare_object(self,obj):
        obj_img = cv2.cvtColor(obj, cv2.COLOR_BGR2GRAY)
        obj_img = (obj_img * 255 / np.max(obj_img)).astype('uint8')
        obj_img = cv2.cvtColor(obj_img,cv2.COLOR_GRAY2BGR)
        obj_img = cv2.morphologyEx(obj_img, cv2.MORPH_OPEN, (15, 15))
        blur = cv2.GaussianBlur(obj_img, (5, 5), 0)
        obj_img = cv2.addWeighted(blur, 1.5, obj_img, -0.5, 0)

        return obj_img

    def __create_custom_color_map(self,lower_col,upper_col):
        lut = np.zeros((256, 1, 3), dtype=np.uint8)

        # lower_col = self.__raise_max(lower_col)
        # upper_col = self.__raise_max(upper_col)
        # lower_col = self.__lower_minmid(lower_col)
        # upper_col = self.__lower_minmid(upper_col)

        for i in range(len(lower_col)):
            arr = np.linspace(lower_col[i], upper_col[i], num=256, dtype="uint8")
            arr[250:] = 255
            lut[:, 0, i] = arr
        return lut

    @staticmethod
    def __raise_max(arr, rate=40):
        arr[np.argmax(arr)] += rate
        if arr[np.argmax(arr)] > 255:
            arr[np.argmax(arr)] = 255
        return arr

    @staticmethod
    def __lower_minmid(arr,rate=40):
        ind = np.argmax(arr)
        for i in range(len(arr)):
            if i == ind:
                continue
            arr[i] -= rate
            if arr[i] < 0:
                arr[i] = 0
        return arr

    def save_background(self,fname):
        show_wait(self.__background,"FINAL")
        cv2.imwrite(fname,self.__background)

class MockGenerator(object):
    def generateObject(self):
        obj = cv2.imread("tmp/blue_580.png")
        return obj

    @staticmethod
    def load_positions(fname):
        with open(fname,"r") as data:
            positions = json.loads(data.read())["objects"]
        return positions

    @staticmethod
    def get_stat_positions(positions):
        analyzed = []
        for pos in positions:
            area = pos[1][1] * pos[1][0]
            pos.append(area)
            analyzed.append(pos)
        analyzed = np.array(analyzed)
        mean = np.mean(analyzed[:,2])
        std = np.std(analyzed[:,2])
        coeff = 2
        right = mean + (coeff*std)
        return(analyzed[np.where(analyzed[:,2]<right)])

if __name__ == "__main__":
    ic = ImageCreator()
    ic.load_background("../1.png")

    gan_blue = GAN()
    gan_blue.load_generator("../blue_gen.h5")
    ic.put_cells(MockGenerator.load_positions("../blue.json"),gan_blue)

