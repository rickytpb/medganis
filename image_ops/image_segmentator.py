import cv2
from utils import show_wait
from sklearn.cluster import KMeans
import json
import time
import numpy as np
import os

class ImageSegmentator(object):
    def __init__(self):
        self.__img = None
        self.__prep_img = None
        self.__imdict = {}
        self.__positions = []
        self.__debug = False
        self.__visual = False
        self.__wsratio = 0.1
        self.__max_area = 10000
        self.__min_area = 20
        self.__obj_no = 0
        self.__last_markers = None

    def set_max_area(self,val):
        self.__max_area = val

    def get_max_area(self):
        return self.__max_area

    def set_min_area(self,val):
        self.__min_area = val

    def get_min_area(self):
        return self.__min_area

    def set_debug_mode(self, val):
        self.__debug = val

    def set_visual_mode(self, val):
        self.__visual = True

    def __display(self, img, imname):
        if self.__visual:
            show_wait(img, imname)

    def __log(self, *args):
        if self.__debug:
            print("DEBUG: " + " ".join(map(str, args)))

    def load_image(self, img_name, resize_ratio=1.):
        self.__log("Loading image", img_name, "resize ratio:", resize_ratio)
        self.__img = cv2.imread(img_name)
        self.__img = cv2.resize(self.__img, None, fx=resize_ratio, fy=resize_ratio)
        self.__display(self.__img, "Loaded image")

    def prepare_image(self,channels, correction_val=50, threshold=60, ):
        self.__log("Converting to LAB")
        lab_img = cv2.cvtColor(self.__img, cv2.COLOR_RGB2LAB)
        self.__display(lab_img, "Image in LAB color space")
        w, h, d = tuple(self.__img.shape)
        self.__log("Removing noise with fastN1MeansDenoisingColored")
        denoised_rgb = cv2.fastNlMeansDenoisingColored(self.__img)
        self.__display(denoised_rgb, "Denoised image")
        self.__log("Altering RGB image by analyzing image in LAB color space")
        rgb_image = denoised_rgb
        for i in range(w):
            for j in range(h):
                for k in range(d):
                    if lab_img[i, j, k] < threshold:
                        for channel in channels:
                            rgb_image[i, j, channel] += correction_val
        self.__prep_img = rgb_image
        self.__log("Finished altering RGB image")
        self.__display(rgb_image, "Prepared RGB image")

    def __recreate_image(self, kmeans, label, proc_im):
        """Recreate the (compressed) image from the code book & labels"""
        w, h, _ = self.__img.shape
        image = np.zeros((w, h, 3))
        colors = []
        label_idx = 0
        for i in range(w):
            for j in range(h):
                if kmeans[label_idx] == label:
                    image[i][j] = self.__img[i][j]
                    colors.append(self.__img[i][j])
                else:
                    image[i][j] = [255, 255, 255]
                label_idx += 1
        image = np.array(image, dtype=np.uint8)
        blue_m = np.array(colors)[:, 0].mean()
        green_m = np.array(colors)[:, 1].mean()
        red_m = np.array(colors)[:, 2].mean()
        return image, [blue_m, green_m, red_m]

    @staticmethod
    def __get_circullar_cells(img, min_size=0, max_size=1000):
        # Setup SimpleBlobDetector parameters.
        params = cv2.SimpleBlobDetector_Params()

        # Change thresholds
        params.minThreshold = 10
        params.maxThreshold = 220

        # Filter by Area.
        params.filterByArea = True
        params.minArea = min_size
        params.maxArea = max_size

        # Filter by Circularity
        params.filterByCircularity = True
        params.minCircularity = 0.1

        # Filter by Convexity
        params.filterByConvexity = True
        params.minConvexity = 0.3

        # Filter by Inertia
        # params.filterByInertia = True
        # params.minInertiaRatio = 0.1
        detector = cv2.SimpleBlobDetector_create(params)
        keypoints = detector.detect(img)

        # Draw detected blobs as red circles.
        # cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS ensures
        # the size of the circle corresponds to the size of blob

        im_with_keypoints = cv2.drawKeypoints(img, keypoints, np.array([]), (0, 0, 255),
                                              cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)

        return keypoints


    def load_prepared_image(self, imname, label, resize=1.):
        self.__imdict[label] = cv2.imread(imname)
        self.__imdict[label] = cv2.resize(self.__imdict[label], None, fx=resize, fy=resize)

    def load_all_prep_images(self, resize=1.):
        labels = ["blue", "red", "background"]
        imnames = ["blue.png", "red.png", "background.png"]
        for label, imname in zip(labels, imnames):
            self.load_prepared_image(imname, label, resize)

    def save_prepared_images(self,dir):
        ts = str(int(time.time()))
        for key in self.__imdict.keys():
            cv2.imwrite(os.path.join(dir,str(key) + '_' + ts + '_' + ".png"), self.__imdict[key])

    def separate_image(self, n_clusters):
        w, h, _ = self.__prep_img.shape
        self.__log("Converting prepared image to LAB space")
        lab_img = cv2.cvtColor(self.__prep_img, cv2.COLOR_RGB2LAB)
        self.__display(lab_img, "Prepared image in LAB space")
        im_array = np.reshape(lab_img[:, :, 1:], (w * h, 2))
        self.__log("Applying K-Means algorithm")
        kmeans = KMeans(n_clusters=n_clusters, random_state=0, n_init=50).fit(im_array)
        self.__log("Recreating clustered images from arrays")
        imgs = []
        for i in range(0,n_clusters):
            img,mean = self.__recreate_image(kmeans.labels_, i, im_array)
            imgs.append(img)
            self.__imdict[i] = img
            self.__display(img,str("Cluster number: %d" % (i)))

    def __watershed_image(self, img, dist_ratio, maskSize=0):
        #It was RGB
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        self.__log("Processing threshold")
        ret, thresh = cv2.threshold(gray, 0, 255, cv2.THRESH_BINARY_INV + cv2.THRESH_OTSU)
        kernel = np.ones((3, 3), np.uint8)
        self.__log("Processing dilate")
        opening = cv2.morphologyEx(thresh, cv2.MORPH_OPEN, kernel, iterations=2)
        sure_bg = cv2.dilate(opening, kernel, iterations=3)
        self.__log("Processing pre watershed transformations")
        dist_transform = cv2.distanceTransform(opening, cv2.DIST_L1,maskSize)
        ret, sure_fg = cv2.threshold(dist_transform, dist_ratio * dist_transform.max(), 255,0)
        sure_fg = np.uint8(sure_fg)
        unknown = cv2.subtract(sure_bg, sure_fg)
        ret, markers = cv2.connectedComponents(sure_fg)
        markers = markers + 1
        markers[unknown == 255] = 0
        markers = cv2.watershed(img, markers)
        return markers

    def __extract_objects(self, img, markers):
        objects = []
        x = []
        y = []
        h_s = []
        v_s = []
        for k, label in enumerate(np.unique(markers)):
            if label == 0:
                continue
            gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
            mask = np.zeros_like(gray, dtype="uint8")
            mask[markers == label] = 255
            mask_col = np.zeros_like(img)
            mask_col[markers == label] = [255, 255, 255]
            cnts = cv2.findContours(mask.copy(), cv2.RETR_EXTERNAL,
                                    cv2.CHAIN_APPROX_SIMPLE)[-2]
            c = max(cnts, key=cv2.contourArea)
            area = cv2.contourArea(c)
            print(area)
            if area>self.__max_area or area<self.__min_area:
                 continue

            r = cv2.boundingRect(c)
            cropped = np.bitwise_and(img, mask_col)
            obj = cropped[r[1]:r[1] + r[3], r[0]:r[0] + r[2]]
            obj[np.where((obj == [0, 0, 0]).all(axis=2))] = [255, 255, 255]
            objects.append(obj)
            x.append(r[0])
            y.append(r[1])
            h_s.append(r[2])
            v_s.append(r[3])
        return objects,x,y,h_s,v_s

    def get_image_by_label(self,label):
        return self.__imdict[label]

    def segmentate_image(self, image, label, dirname="", wsratio=None,x_offset=0,y_offset=0):
        self.__log("Watersheding")
        if wsratio is None:
            wsratio = self.__wsratio
        markers = self.__watershed_image(image, wsratio)
        if isinstance(self.__last_markers,np.ndarray):
            i = 0.05
            while(self.__last_markers.all() == markers.all()) and wsratio+i<0.95:
                markers = self.__watershed_image(image,wsratio+i)
                i += 0.05
        self.__last_markers = markers
        self.__log("Starting object detection")
        objects,xs,ys,h_sizes,v_sizes = self.__extract_objects(image,markers)
        for obj,x,y,h_size,v_size in (zip(objects,xs,ys,h_sizes,v_sizes)):
            border_size = 10
            obj_wborder = cv2.copyMakeBorder(obj, border_size, border_size,
                                             border_size, border_size, cv2.BORDER_CONSTANT, value=(255, 255, 255))
            keypoints = self.__get_circullar_cells(obj_wborder,self.__min_area)

            if len(keypoints) == 0:
                self.__log("No keypoints found")
                continue

            if len(keypoints) == 1:
                coord = (int(x +h_size/2 + x_offset), int(y + v_size/2 + y_offset))
                size = (h_size,v_size)
                self.__positions.append([coord,size])
                object_name = '%s/%s_%d.png' % (dirname, label, self.__obj_no)
                self.__log("Saving object %s" % object_name)
                cv2.imwrite(object_name, obj_wborder)
                self.__obj_no +=1
                continue

            if len(keypoints) > 1:
                self.__log("Multiple keypoints found")
                self.segmentate_image(obj_wborder,dirname,wsratio=0.3,x_offset=x+x_offset-border_size,y_offset=y+y_offset-border_size)
        self.__last_markers = None
        return len(self.__positions)

    def reset_obj_counter(self):
        self.__obj_no = 0

    def save_positions(self,label,dirname="tmp"):
        json_data = {"objects": self.__positions}
        with open(os.path.join(dirname,str(label)+".json"),"w") as data:
            data.write(json.dumps(json_data))

    @staticmethod
    def create_image(positions,bg,show=True):
        bg_img = cv2.imread(bg)
        for pos in positions:
            cv2.circle(bg_img,(pos[0][0],pos[0][1]), 5, (0,0,255), 1)
        if show:
            show_wait(bg_img,"created image")




if __name__ == "__main__":
    # s = ImageSegmentator()
    # s.set_debug_mode(True)
    # s.set_visual_mode(True)
    # img = cv2.imread("../2.png")
    # s.segmentate_image(img,"blu","tmp")
    # s.save_positions("test")
    positions = ImageSegmentator.get_stat_positions("test.json")
    ImageSegmentator.create_image(positions,"../2.png")

    # s.load_image("../medganis/1M02_samp2.tif", resize_ratio=0.14)
    # s.prepare_image([2])
    # s.separate_image(3)
    #s.save_prepared_images()
    # s.load_all_prep_images()
    # blue = s.get_image_by_label("blue")
    # red = s.get_image_by_label("red")
    # print("BLUE")
    # s.segmentate_image(blue, "blue",dirname="tmp")
    # print("RED")
    # s.segmentate_image(red, "red", dirname="tmp")
    # s.save_positions("cell_pos.json")