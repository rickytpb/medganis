import cv2
import sys

def except_hook(cls, exception, traceback):
    sys.__excepthook__(cls, exception, traceback)

def show_wait(img, imname):
    cv2.startWindowThread()
    cv2.imshow(imname, img)
    k = cv2.waitKey(0)
    if k == 27:  # wait for ESC key to exit
        cv2.destroyAllWindows()